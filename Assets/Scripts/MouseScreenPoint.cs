﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class MouseScreenPoint : MonoBehaviour
{
    public UnityEvent OnMouseClicked = new UnityEvent();
    public UnityEvent OnMousePressed = new UnityEvent();
    public UnityEvent OnMouseReleased = new UnityEvent();

    public bool initialClick;
    public bool drawing;
    public bool released;
    public bool canRegisterInput;

    private void Update()
    {
        if (canRegisterInput)
        {
            // Store the initial click of the mouse
            if (Input.GetMouseButtonDown(0) && !initialClick)
            {
                initialClick = true;
                OnMouseClicked?.Invoke();

                drawing = true;
            }

            // Storing the mouse coordinates when mouse button being pressed
            if (Input.GetMouseButton(0) && drawing)
            {
                OnMousePressed?.Invoke();
            }

            // Finish drawing line when mouse button is released
            if (Input.GetMouseButtonUp(0))
            {
                drawing = false;
                OnMouseReleased?.Invoke();
                initialClick = false;
                canRegisterInput = false;
            }
        }
    }

    // Return the x,y screen coordinates where the mouse is clicking
    public Vector2 GetScreenPoint()
    {
        return Camera.main.ScreenToWorldPoint(Input.mousePosition);
    }
}